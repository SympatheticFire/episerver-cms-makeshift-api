﻿using System;
using System.IO;
using System.Net;
using System.Text;
using System.Windows.Forms;

namespace RyzStudio.Net
{
	public class HttpWeb
	{
		public string UserAgent { get; set; } = "Momozilla/5.0 (" + Environment.OSVersion.Platform.ToString() + " ; " + Environment.OSVersion.VersionString + "; " + Application.CurrentCulture.TwoLetterISOLanguageName + ")";
		public int Timeout { get; set; } = 6000;
		public int MaxRedirect { get; set; } = 8;
		public bool AllowRedirect { get; set; } = true;
		public CookieContainer CookierJar { get; set; } = null;

		public HttpWeb()
		{
		}

		public HttpWebRequest CreateRequest(string url)
		{
			return this.CreateRequest(url, url);
		}

		public HttpWebRequest CreateRequest(string url, string referrerURL)
		{
			if (CookierJar == null)
			{
				CookierJar = new CookieContainer();
			}

			HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(url);
			webRequest.CachePolicy = new System.Net.Cache.RequestCachePolicy(System.Net.Cache.RequestCacheLevel.NoCacheNoStore);
			webRequest.MaximumAutomaticRedirections = MaxRedirect;
			webRequest.CookieContainer = CookierJar;
			webRequest.UserAgent = UserAgent;
			webRequest.AllowAutoRedirect = AllowRedirect;
			webRequest.Timeout = Timeout;

			return webRequest;
		}

		public int GetResponse(out string sourceCode, string url, string referrerURL = "")
		{
			HttpWebRequest webRequest = this.CreateRequest(url, referrerURL);

			return GetResponse(out sourceCode, webRequest);
		}

		public int GetResponse(out string sourceCode, HttpWebRequest webRequest)
		{
			sourceCode = string.Empty;

			int rv = 0;

			try
			{
				HttpWebResponse webResponse = (HttpWebResponse)webRequest.GetResponse();

				rv = (int)webResponse.StatusCode;

				StreamReader readContent = new StreamReader(webResponse.GetResponseStream());
				sourceCode = readContent.ReadToEnd();

				webResponse.Close();
				webResponse = null;
			}
			catch (WebException xc)
			{
				if (xc.Response is HttpWebResponse)
				{
					HttpWebResponse rs = xc.Response as HttpWebResponse;
					StreamReader readContent = new StreamReader(rs.GetResponseStream());
					if (readContent != null)
					{
						sourceCode = readContent.ReadToEnd();
					}

					rv = (int)rs.StatusCode;
				}
				else
				{
					rv = (int)xc.Status;
					sourceCode = xc.Message;
				}
			}
			catch (Exception xc)
			{
				sourceCode = xc.Message;
			}

			return rv;
		}

		public static HttpWebRequest AddBasicAuthentication(HttpWebRequest webRequest, string username, string password)
		{
			webRequest.Headers["Authorization"] = "Basic " + Convert.ToBase64String(Encoding.Default.GetBytes(string.Concat(username, ":", password)));
			webRequest.PreAuthenticate = true;

			return webRequest;
		}


		public int GetPOSTResponse(out string sourceCode, HttpWebRequest webRequest, string postData)
		{
			sourceCode = "";
			int rv = 0;
			byte[] buffer = Encoding.UTF8.GetBytes(postData);

			webRequest.ContentLength = buffer.Length;

			try
			{
				Stream dataStream = webRequest.GetRequestStream();
				dataStream.Write(buffer, 0, buffer.Length);
				dataStream.Close();
			}
			catch (Exception xc)
			{
				sourceCode = xc.Message;
				return rv;
			}

			return this.GetResponse(out sourceCode, webRequest);
		}
	}
}
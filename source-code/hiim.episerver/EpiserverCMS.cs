﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using HtmlAgilityPack;
using Newtonsoft.Json;
using RyzStudio.Net;

namespace RyzStudio.Web.Episerver
{
    public class EpiserverCMS
	{
		protected const string PATH_SHELL = "/EPiServer/Shell/";

		protected const string PATH_LOGIN = "/Util/login.aspx";

		protected const string BASE = "/EPiServer";

		protected const string CMS_STORES = BASE + "/cms/Stores";
		protected const string CMS_ADMIN = BASE + "/CMS/Admin/";
		protected const string SHELL_STORES = BASE + "/shell/Stores";
		protected const string CHANNEL_GETALLREGISTERED = CMS_STORES + "/channel/?query=getallregistered&dojo.preventCache={0}";
		protected const string COMPONENT_VIEWNAME = SHELL_STORES + "/component/?viewName=%2Fepiserver%2Fcms%2Fhome&dojo.preventCache={0}";
		protected const string CONTENTDATA = CMS_STORES + "/contentdata/{1}?dojo.preventCache={0}";
		protected const string CONTENTSTRUCTURE = CMS_STORES + "/contentstructure/{1}?dojo.preventCache={0}";
		protected const string CONTENTSTRUCTURE_GETPAGECHILDREN = CMS_STORES + "/contentstructure/?referenceId={1}&query=getchildren&typeIdentifiers=episerver.core.pagedata&allLanguages=true&sort()&dojo.preventCache={0}";
		protected const string CONTENTTYPE = CMS_STORES + "/contenttype/?dojo.preventCache={0}";
		protected const string CONTENTVERSION = CMS_STORES + "/contentversion/{1}?dojo.preventCache={0}";
		protected const string CONTEXT = SHELL_STORES + "/context/?uri=epi.cms.contentdata%3A%2F%2F%2F{1}&dojo.preventCache={0}";
		protected const string LANGUAGE = CMS_STORES + "/language/?dojo.preventCache={0}";
		protected const string LICENSEINFORMATION = SHELL_STORES + "/licenseinformation/?dojo.preventCache={0}";
		protected const string METADATA = SHELL_STORES + "/metadata/EPiServer.Core.ContentData?modelAccessor=%7B%22contentLink%22%3A%22{1}%22%7D&dojo.preventCache={0}";
		protected const string PROFILE_USERNAME = SHELL_STORES + "/profile/userName?dojo.preventCache={0}";
		protected const string PROFILE_CURRENTPROJECTID = SHELL_STORES + "/profile/epi.current-project-id?dojo.preventCache={0}";
		protected const string SITESTRUCTURE = CMS_STORES + "/sitestructure/?dojo.preventCache={0}";
		protected const string SITESTRUCTURE_SITEURL = CMS_STORES + "/sitestructure/?siteUrl={1}&allLanguages=true&dojo.preventCache={0}";
		protected const string UIDESCRIPTOR = SHELL_STORES + "/uidescriptor/?dojo.preventCache={0}";
		protected const string VISITORGROUP = CMS_STORES + "/visitorgroup/?dojo.preventCache={0}";
		
		public HttpWeb WebClient { get; set; }
		public string BaseURL { get; set; } = @"http://localhost:5156";
		public string BaseURLCMS { get; set; } = @"http://localhost:5156";

////		protected string version = "";
		protected bool isLoggedIn = false;

		public EpiserverCMS()
        {
			this.WebClient = new HttpWeb();
        }

		#region public properties (read-only)

		public bool IsLoggedIn { get { return isLoggedIn; } }

////		public string Version { get { return version; } }

		#endregion

		#region public methods

		public bool Login(string username, string password)
        {
            string postData = buildLoginPostData(username, password);
			if (string.IsNullOrWhiteSpace(postData))
			{
				isLoggedIn = false;
				return false;
			}

            HttpWebRequest webRequest = this.WebClient.CreateRequest(BaseURLCMS + PATH_LOGIN);
			webRequest.ServerCertificateValidationCallback = delegate { return true; };
			webRequest.ContentType = "application/x-www-form-urlencoded";
			webRequest.Method = "POST";

            string sourceCode;
            int statusCode = this.WebClient.GetPOSTResponse(out sourceCode, webRequest, postData);
			if (isLoggedIn && (statusCode == 500))
			{
				return true;
			}

            if (statusCode != 200) 
            {
				isLoggedIn = false;
				return false;
            }

			string profileUsername = this.GetProfile_Username();
			if (string.IsNullOrWhiteSpace(profileUsername))
			{
				isLoggedIn = false;
				return false;
			}

			isLoggedIn = true;
			return true;
        }

		public void Logout()
		{
			isLoggedIn = false;
			WebClient.CookierJar = new CookieContainer();
		}

		public List<Channel> GetChannel_GetAllRegistered() { return retrieveResponse<List<Channel>>(string.Format(CHANNEL_GETALLREGISTERED, DateTime.Now.Ticks.ToString())); }

		public List<Component> GetComponent_ViewName() { return retrieveResponse<List<Component>>(string.Format(COMPONENT_VIEWNAME, DateTime.Now.Ticks.ToString())); }

		public ContentData GetContentData(string id) { return retrieveResponse<ContentData>(string.Format(CONTENTDATA, DateTime.Now.Ticks.ToString(), id)); }

		public ContentStructure GetContentStructure(int id) { return retrieveResponse<ContentStructure>(string.Format(CONTENTSTRUCTURE, DateTime.Now.Ticks.ToString(), id.ToString())); }

		public List<ContentStructure> GetContentStructure_GetPageChildren(string id)
		{
			return retrieveResponse<List<ContentStructure>>(string.Format(CONTENTSTRUCTURE_GETPAGECHILDREN, DateTime.Now.Ticks.ToString(), id));
		}

		public List<ContentType> GetContentType() { return retrieveResponse<List<ContentType>>(string.Format(CONTENTTYPE, DateTime.Now.Ticks.ToString())); }

		public ContentVersion GetContentVersion(string id) { return retrieveResponse<ContentVersion>(string.Format(CONTENTVERSION, DateTime.Now.Ticks.ToString(), id)); }

		public Context GetContext(string id) { return retrieveResponse<Context>(string.Format(CONTEXT, DateTime.Now.Ticks.ToString(), id)); }

		public LicenseInformation GetLicenseInformation() { return retrieveResponse<LicenseInformation>(string.Format(LICENSEINFORMATION, DateTime.Now.Ticks.ToString())); }

		public List<Language> GetLanguage() { return retrieveResponse<List<Language>>(string.Format(LANGUAGE, DateTime.Now.Ticks.ToString())); }

		public string GetProfile_Username()
		{
			IDValue rs = retrieveResponse<IDValue>(string.Format(PROFILE_USERNAME, DateTime.Now.Ticks.ToString()));
			return ((rs == null) ? string.Empty : ((rs.Value == null) ? string.Empty : (rs.Value as string)));
		}

		public int GetProfile_CurrentProjectID()
		{
			IDValue rs = retrieveResponse<IDValue>(string.Format(PROFILE_CURRENTPROJECTID, DateTime.Now.Ticks.ToString()));
			string rv = ((rs == null) ? string.Empty : ((rs.Value == null) ? string.Empty : (rs.Value as string)));

			int n;
			if (!int.TryParse(rv, out n))
			{
				n = 0;
			}

			return n;
		}

		public List<SiteStructure> GetSiteStructure() { return retrieveResponse<List<SiteStructure>>(string.Format(SITESTRUCTURE, DateTime.Now.Ticks.ToString())); }

		public List<SiteStructure> GetSiteStructure_SiteURL(string siteURL) { return retrieveResponse<List<SiteStructure>>(string.Format(SITESTRUCTURE_SITEURL, DateTime.Now.Ticks.ToString(), HttpUtility.UrlEncode(siteURL))); }

		public List<UIDescriptor> GetUIDescriptor() { return retrieveResponse<List<UIDescriptor>>(string.Format(UIDESCRIPTOR, DateTime.Now.Ticks.ToString())); }

		public string GetVersion()
		{
			HttpWebRequest webRequest = this.WebClient.CreateRequest(BaseURLCMS + CMS_ADMIN);

			string sourceCode;
			int statusCode = this.WebClient.GetResponse(out sourceCode, webRequest);
			if (statusCode != 200)
			{
				return null;
			}

			HtmlAgilityPack.HtmlDocument document = new HtmlDocument();
			document.LoadHtml(sourceCode);

			HtmlNode hn = document.DocumentNode.SelectSingleNode("//title");
			if (hn == null)
			{
				return null;
			}

			string title = hn.InnerText;
			title = title.Substring(title.LastIndexOf("CMS ") + 4);
			title = title.Substring(0, title.IndexOf("("));

			return title.Trim();
		}

		public List<IDName> GetVisitorGroup() { return retrieveResponse<List<IDName>>(string.Format(VISITORGROUP, DateTime.Now.Ticks.ToString())); }

		#endregion

		protected string buildLoginPostData(string username, string password)
        {
			string sourceCode;
			int statusCode = this.WebClient.GetResponse(out sourceCode, BaseURLCMS + PATH_LOGIN, BaseURLCMS + PATH_LOGIN);
			if (statusCode != 200)
            {
                return null;
            }

            HtmlAgilityPack.HtmlDocument document = new HtmlAgilityPack.HtmlDocument();
            document.LoadHtml(sourceCode);

            StringBuilder sb = new StringBuilder();
            sb.Append("LoginControl%24Button1=Log+in");

            HtmlNodeCollection nodeList = document.DocumentNode.SelectNodes("//input[@type='hidden']");
            if (nodeList != null)
            {
                foreach (HtmlNode node in nodeList)
                {
                    sb.Append("&");
                    sb.Append(HttpUtility.UrlEncode((node.Attributes["name"] == null) ? string.Empty : node.Attributes["name"].Value));
                    sb.Append("=");
                    sb.Append(HttpUtility.UrlEncode((node.Attributes["value"] == null) ? string.Empty : node.Attributes["value"].Value));
                }                
            }

            sb.Append("&LoginControl%24UserName=" + HttpUtility.UrlEncode(username));
            sb.Append("&LoginControl%24Password=" + HttpUtility.UrlEncode(password));
            
            return sb.ToString();
        }

        protected string decodeUTF8(string rawText)
        {
            Encoding utf8 = new UTF8Encoding();
            Encoding window1252 = Encoding.GetEncoding("Windows-1252");

            byte[] postBytes = window1252.GetBytes(rawText.Trim());

            return HttpUtility.HtmlDecode(utf8.GetString(postBytes)).Replace("\n", "").Replace("\r", "").Replace("\t", "").Trim();
        }

		protected T decodeJSON<T>(string sourceCode)
		{
			if (string.IsNullOrWhiteSpace(sourceCode))
			{
				return default(T);
			}

			string sc = (sourceCode.StartsWith("{}&&") ? sourceCode.Substring(4).Trim() : sourceCode.Trim());
			if (string.IsNullOrWhiteSpace(sc))
			{
				return default(T);
			}

			T rs = default(T);

			try
			{
				rs = JsonConvert.DeserializeObject<T>(sc);
				if (rs == null)
				{
					return default(T);
				}
			}
			catch (Exception exc)
			{
				return default(T);
			}

			return rs;
		}

		protected T retrieveResponse<T>(string path)
		{
			string actionURL = string.Concat(BaseURLCMS, path);

			string sourceCode;
			int statusCode = this.WebClient.GetResponse(out sourceCode, actionURL, actionURL);
			if (statusCode != 200)
			{
				return default(T);
			}

			T rs = decodeJSON<T>(sourceCode);
			if (rs == null)
			{
				return default(T);
			}

			return rs;
		}

		/// <summary>
		/// Get Version
		/// </summary>
		/// <param name="node">Document root node</param>
/*
		protected void setVersion(HtmlNode node)
		{
			version = string.Empty;

			if (node == null)
			{
				return;
			}

			HtmlNodeCollection hnc = node.SelectNodes("//link[@rel='stylesheet'][@href]");
			if (hnc == null)
			{
				return;
			}

			HtmlNode hn = hnc.Where(x => ((x.Attributes["href"] == null) ? string.Empty : x.Attributes["href"].Value).StartsWith(PATH_SHELL)).FirstOrDefault();
			if (hn == null)
			{
				return;
			}

			string rv = hn.Attributes["href"].Value.Substring(PATH_SHELL.Length);
			if (!rv.Contains("/"))
			{
				return;
			}

			version = rv.Substring(0, rv.IndexOf("/"));
		}
*/

    }
}

﻿namespace EPiServer.Core
{
	public enum ContentProviderCapabilities
	{
		None = 0,
		Create = 1,
		Edit = 2,
		Delete = 4,
		Move = 8,
		Copy = 16,
		MultiLanguage = 32,
		Security = 64,
		Search = 128,
		PageFolder = 256,
		Wastebasket = 512
	}
}

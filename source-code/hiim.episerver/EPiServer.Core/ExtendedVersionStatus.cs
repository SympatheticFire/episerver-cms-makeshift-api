﻿namespace EPiServer.Core
{
	public enum ExtendedVersionStatus
	{
		Expired = 100,
		NotCreated = 0,
		Rejected,
		CheckedOut,
		CheckedIn,
		Published,
		PreviouslyPublished,
		DelayedPublish,
		AwaitingApproval
	}
}

﻿namespace RyzStudio.Web.Episerver
{
	public class Settings
	{
		public int Column { get; set; }
		public object LastOpenHeight { get; set; }
		public bool Open { get; set; }
		public object LiveSplitters { get; set; }
		public string Style { get; set; }
		public bool Gutters { get; set; }
		public string ID { get; set; }
		public string Region { get; set; }
		public int? MinSize { get; set; }
		public string Splitter { get; set; }
		public int? NumberOfColumns { get; set; }
		public object ShowToolbar { get; set; }
		public object ContainerUnlocked { get; set; }
		public object Closable { get; set; }
		public string PersonalizableHeading { get; set; }
		public string HeadingLocalizationKey { get; set; }
		public string Heading { get; set; }
		public string RepositoryKey { get; set; }
		public Query[] Queries { get; set; }
		public Category[] Categories { get; set; }
		public string DndFormatSuffix { get; set; }
		public string URL { get; set; }
		public int? Size { get; set; }
	}
}

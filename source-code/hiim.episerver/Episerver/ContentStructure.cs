﻿using EPiServer.Core;
using System;
using System.Collections.Generic;

namespace RyzStudio.Web.Episerver
{
	public class ContentStructure
	{
		public bool IsPreferredLanguageAvailable { get; set; }
		public object MissingLanguageBranch { get; set; }
		public bool HasTranslationAccess { get; set; }
		public bool IsSubRoot { get; set; }
		public bool IsStartPage { get; set; }
		public bool IsWastebasket { get; set; }
		public bool HasChildren { get; set; }
		public string ThumbnailURL { get; set; }
		public string PublicURL { get; set; }
		public string DownloadURL { get; set; }
		public string PreviewURL { get; set; }
		public string PermanentLink { get; set; }
		public bool HasTemplate { get; set; }
		public int AccessMask { get; set; }
		public int AccessRights { get; set; }
		public string Name { get; set; }
		public string ContentLink { get; set; }
		public string ParentLink { get; set; }
		public string URI { get; set; }
		public string ContentGUID { get; set; }
		public int ContentTypeID { get; set; }
		public string ContentTypeName { get; set; }
		public LanguageBranch CurrentLanguageBranch { get; set; }
		public string AssetsFolderLink { get; set; }
		public PageProperties Properties { get; set; }
		public List<object> InUseNotifications { get; set; }
		public DateTime Created { get; set; }
		public string CreatedBy { get; set; }
		public DateTime Changed { get; set; }
		public string ChangedBy { get; set; }
		public string DeletedBy { get; set; }
		public DateTime? Deleted { get; set; }
		public DateTime Saved { get; set; }
		public bool IsDeleted { get; set; }
		public IDictionary<string, bool> Capabilities { get; set; }
		public ContentProviderCapabilities ProviderCapabilityMask { get; set; }
		public ExtendedVersionStatus Status { get; set; }
		public string TypeIdentifier { get; set; }
		public object OwnerTypeIdentifier { get; set; }
		public object OwnerContentLink { get; set; }
	}
}

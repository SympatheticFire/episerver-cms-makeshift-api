﻿namespace RyzStudio.Web.Episerver
{
	public class Channel
	{
		public bool IsActive { get; set; }
		public string ChannelName { get; set; }
		public string ResolutionID { get; set; }
		public string Name { get; set; }
	}
}

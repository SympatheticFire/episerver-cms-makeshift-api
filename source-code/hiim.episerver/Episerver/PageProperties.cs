﻿using System;

namespace RyzStudio.Web.Episerver
{
	public class PageProperties
	{
		public string PageLanguageBranch { get; set; }
		public object PageExternalURL { get; set; }
		public int PageChildOrderRule { get; set; }
		public DateTime PageSaved { get; set; }
		public string PageChangedBy { get; set; }
		public DateTime PageCreated { get; set; }
		public DateTime PageChanged { get; set; }
		public int PageTypeID { get; set; }
		public bool PageVisibleInMenu { get; set; }
		public int PageShortcutType { get; set; }
		public object PageShortcutLink { get; set; }
		public string PageLinkURL { get; set; }
	}
}

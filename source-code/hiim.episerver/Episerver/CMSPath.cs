﻿namespace RyzStudio.Web.Episerver
{
	public class CMSPath
	{
		public string Key { get; set; }
		public string Name { get; set; }
		public string Description { get; set; }
		public string LanguagePath { get; set; }
		public string IconClass { get; set; }
		public bool HideFromViewMenu { get; set; }
		public string ViewType { get; set; }
		public string ControllerType { get; set; }
		public int SortOrder { get; set; }
		public object Category { get; set; }
		public object PlugInAreas { get; set; }
	}
}

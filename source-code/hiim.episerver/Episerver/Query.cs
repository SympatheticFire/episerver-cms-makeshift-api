﻿namespace RyzStudio.Web.Episerver
{
	public class Query
	{
		public string DisplayName { get; set; }
		public string Name { get; set; }
		public bool VersionSpecific { get; set; }
	}
}

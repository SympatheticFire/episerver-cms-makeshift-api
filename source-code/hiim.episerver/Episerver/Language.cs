﻿namespace RyzStudio.Web.Episerver
{
	public class Language
	{
        public string CultureName { get; set; }
        public string Name { get; set; }
        public string LanguageID { get; set; }
        public bool IsAvailable { get; set; }
        public bool HasEditAccess { get; set; }
        public object EditURL { get; set; }
        public object URL { get; set; }
        public string URLSegment { get; set; }
        public object UiURL { get; set; }
        public bool IsLanguageNode { get; set; }
        public object Host { get; set; }
        public object Hosts { get; set; }
    }
}
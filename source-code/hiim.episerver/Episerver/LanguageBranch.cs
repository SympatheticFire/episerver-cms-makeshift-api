﻿namespace RyzStudio.Web.Episerver
{
	public class LanguageBranch
	{
		public string Name { get; set; }
		public string LanguageID { get; set; }
		public bool IsMasterLanguage { get; set; }
		public object CommonDraftLink { get; set; }
		public object URLSegment { get; set; }
		public int AccessMask { get; set; }
	}
}

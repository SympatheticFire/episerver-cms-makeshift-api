﻿namespace RyzStudio.Web.Episerver
{
	public class SortKey
	{
		public string ColumnName { get; set; }
		public bool SortDescending { get; set; }
	}
}

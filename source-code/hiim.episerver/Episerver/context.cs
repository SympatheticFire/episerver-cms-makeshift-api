﻿namespace RyzStudio.Web.Episerver
{
	public class Context
	{
		public string ParentLink { get; set; }
		public string CustomViewType { get; set; }
		public string Language { get; set; }
		public string PublicURL { get; set; }
		public string EditablePreviewURL { get; set; }
		public Capabilities Capabilities { get; set; }
		public object LanguageContext { get; set; }
		public bool HasSiteChanged { get; set; }
		public string FullHomeURL { get; set; }
		public object VersionedURL { get; set; }
		public string DataType { get; set; }
		public bool HasTemplate { get; set; }
		public string URI { get; set; }
		public string PreviewURL { get; set; }
		public object Data { get; set; }
		public string Name { get; set; }
		public string RequestedURI { get; set; }
		public string VersionAgnosticURI { get; set; }
	}
}

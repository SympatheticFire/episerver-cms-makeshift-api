﻿namespace RyzStudio.Web.Episerver
{
	public class IDName
	{
		public string ID { get; set; }
		public string Name { get; set; }
	}
}

﻿namespace RyzStudio.Web.Episerver
{
	public class VersionableShortcut
	{
		public int PageShortcutType { get; set; }
		public string PageShortcutTypeName { get; set; }
		public object PageTargetFrame { get; set; }
		public object ContentShortcutLink { get; set; }
		public string PageLinkURL { get; set; }
	}
}

﻿namespace RyzStudio.Web.Episerver
{
	public class Component
	{
		public string ViewName { get; set; }
		public string ParentID { get; set; }
		public string ID { get; set; }
		public Settings Settings { get; set; }
		public string DefinitionName { get; set; }
		public string WidgetType { get; set; }
		public string ModuleName { get; set; }
		public int SortOrder { get; set; }
		public object[] Components { get; set; }
		public int ContainerType { get; set; }
		public object PlugInArea { get; set; }
	}
}

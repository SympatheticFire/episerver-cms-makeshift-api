﻿namespace RyzStudio.Web.Episerver
{
	public class VersionableExpire
	{
		public object StopPublish { get; set; }
		public string ArchiveLink { get; set; }
	}
}

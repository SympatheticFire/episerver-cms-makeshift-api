﻿namespace RyzStudio.Web.Episerver
{
	public class LicenseInformation
	{
		public string ID { get; set; } = null;
		public bool IsValid { get; set; } = false;
		public string Message { get; set; } = "";
		public string AdditionalInformation { get; set; } = "";
	}
}

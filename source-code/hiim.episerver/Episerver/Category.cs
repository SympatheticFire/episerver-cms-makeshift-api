﻿namespace RyzStudio.Web.Episerver
{
	public class Category
	{
		public string DisplayName { get; set; }
		public Query[] Queries { get; set; }
	}
}

﻿namespace RyzStudio.Web.Episerver
{
	public class UIDescriptor
	{
		public int EditorDropBehaviour { get; set; }
		public string TypeIdentifier { get; set; }
		public string BaseTypeIdentifier { get; set; }
		public string IconClass { get; set; }
		public object ContainerTypeIdentifier { get; set; }
		public string[] ContainerTypes { get; set; }
		public object MainWidgetType { get; set; }
		public string[] DisabledViews { get; set; }
		public string[] DndTypes { get; set; }
		public string LanguageKey { get; set; }
		public string DefaultView { get; set; }
		public bool? EnableStickyView { get; set; }
		public object CreateView { get; set; }
		public object PublishView { get; set; }
		public AvailableView[] AvailableViews { get; set; }
		public string CommandIconClass { get; set; }
		public SortKey SortKey { get; set; }
		public bool IsPrimaryType { get; set; }
		public string[] BaseTypes { get; set; }
		public bool? ActAsAnAsset { get; set; }
		public bool? SupportsOpenCssClass { get; set; }
		public bool? AllowSelectItSelf { get; set; }
	}
}

﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RyzStudio.Web.Episerver
{
	public class ContentProperties
	{
		[JsonProperty("icontent_name")]
		public string IContentName { get; set; }

		[JsonProperty("icontent_contentlink")]
		public string IContentContentlink { get; set; }

		[JsonProperty("icontent_parentlink")]
		public string IContentParentlink { get; set; }

		[JsonProperty("icontent_contenttypeid")]
		public int IContentContentTypeID { get; set; }

		[JsonProperty("icontent_isdeleted")]
		public bool IContentIsDeleted { get; set; }

		[JsonProperty("icategorizable_category")]
		public object[] ICategorizableCategory { get; set; }

		[JsonProperty("iversionable_status")]
		public int IVersionableStatus { get; set; }

		[JsonProperty("iversionable_ispendingpublish")]
		public bool IVersionableIsPendingPublish { get; set; }

		[JsonProperty("iversionable_startpublish")]
		public string IVersionableStartPublish { get; set; }

		[JsonProperty("iversionable_stoppublish")]
		public object IVersionableStopPublish { get; set; }

		[JsonProperty("iversionable_shortcut")]
		public VersionableShortcut IVersionableShortcut { get; set; }

		[JsonProperty("iversionable_expire")]
		public VersionableExpire IVersionableExpire { get; set; }

		[JsonProperty("ichangetrackable_created")]
		public string IChangeTrackableCreated { get; set; }

		[JsonProperty("ichangetrackable_createdby")]
		public string IChangeTrackableCreatedBy { get; set; }

		[JsonProperty("ichangetrackable_changed")]
		public string IChangeTrackableChanged { get; set; }

		[JsonProperty("ichangetrackable_setchangedonpublish")]
		public bool IChangeTrackableSetChangedOnPublish { get; set; }

		[JsonProperty("ichangetrackable_changedby")]
		public string IChangeTrackableChangedBy { get; set; }

		[JsonProperty("ichangetrackable_saved")]
		public string IChangeTrackableSaved { get; set; }

		[JsonProperty("ichangetrackable_deletedby")]
		public string IChangeTrackableDeletedBy { get; set; }

		[JsonProperty("ichangetrackable_deleted")]
		public object IChangeTrackableDeleted { get; set; }

		[JsonProperty("iroutable_routesegment")]
		public string IRoutableRouteSegment { get; set; }

		public string PageMasterLanguageBranch { get; set; }
		public string PageLanguageBranch { get; set; }
		public object PageContentAssetsID { get; set; }
		public object PageContentOwnerID { get; set; }
		public bool PageVisibleInMenu { get; set; }
		public int PageChildOrderRule { get; set; }
		public int PagePeerOrder { get; set; }
		public object PageExternalURL { get; set; }
		public object PageArchiveLink { get; set; }
		public int PageShortcutType { get; set; }
		public object PageShortcutLink { get; set; }
		public object PageTargetFrame { get; set; }
		public string PageLinkURL { get; set; }
	}
}

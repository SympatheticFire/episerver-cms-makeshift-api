﻿namespace RyzStudio.Web.Episerver
{
	public class Capabilities
	{
		public bool DeleteLanguageBranch { get; set; }
		public bool Versionable { get; set; }
		public bool DynamicProperties { get; set; }
		public bool IsPage { get; set; }
		public bool LanguageSettings { get; set; }
		public bool Resourceable { get; set; }
		public bool Language { get; set; }
		public bool Securable { get; set; }
		public bool CanConfigureApproval { get; set; }
	}
}

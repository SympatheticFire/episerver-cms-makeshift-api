﻿namespace RyzStudio.Web.Episerver
{
	public class ContentVersion
	{
		public bool IsCommonDraft { get; set; }
		public bool AllowToDelete { get; set; }
		public string ContentLink { get; set; }
		public string Language { get; set; }
		public string Name { get; set; }
		public string SavedBy { get; set; }
		public string StatusChangedBy { get; set; }
		public string SavedDate { get; set; }
		public string URI { get; set; }
		public int Status { get; set; }
		public string TypeIdentifier { get; set; }
	}
}

﻿using System;
using System.Collections.Generic;

namespace RyzStudio.Web.Episerver
{
	public class ContentData
	{
		public string EditablePreviewURL { get; set; }
		public bool IsNewVersionRequired { get; set; }
		public bool IsCommonDraft { get; set; }
		public bool IsMasterVersion { get; set; }
		public string PublishedBy { get; set; }
		public DateTime LastPublished { get; set; }
		public object VersionCreatedBy { get; set; }
		public DateTime VersionCreatedTime { get; set; }
		public bool VisibleToEveryOne { get; set; }
		public bool IsPendingPublish { get; set; }
		public List<LanguageBranch> ExistingLanguageBranches { get; set; }
		public bool IsPartOfAnotherProject { get; set; }
		public List<object> Transitions { get; set; }
		public string PublicURL { get; set; }
		public string DownloadURL { get; set; }
		public string PreviewURL { get; set; }
		public string PermanentLink { get; set; }
		public bool HasTemplate { get; set; }
		public int AccessMask { get; set; }
		public int AccessRights { get; set; }
		public string Name { get; set; }
		public string ContentLink { get; set; }
		public string ParentLink { get; set; }
		public string URI { get; set; }
		public string ContentGUID { get; set; }
		public int ContentTypeID { get; set; }
		public string contentTypeName { get; set; }
		public LanguageBranch CurrentLanguageBranch { get; set; }
		public string AssetsFolderLink { get; set; }
		public ContentProperties Properties { get; set; }
		public List<object> InUseNotifications { get; set; }
		public DateTime Created { get; set; }
		public string CreatedBy { get; set; }
		public DateTime Changed { get; set; }
		public string ChangedBy { get; set; }
		public string DeletedBy { get; set; }
		public object Deleted { get; set; }
		public DateTime Saved { get; set; }
		public bool IsDeleted { get; set; }
		public Capabilities Capabilities { get; set; }
		public int ProviderCapabilityMask { get; set; }
		public int Status { get; set; }
		public string TypeIdentifier { get; set; }
		public object OwnerTypeIdentifier { get; set; }
		public object OwnerContentLink { get; set; }
	}
}

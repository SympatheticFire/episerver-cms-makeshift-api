﻿namespace RyzStudio.Web.Episerver
{
	public class ContentType
	{
		public int ID { get; set; }
		public string GUID { get; set; }
		public string Name { get; set; }
		public string LocalizedName { get; set; }
		public string Description { get; set; }
		public string LocalizedDescription { get; set; }
		public string ImagePath { get; set; }
		public string GroupName { get; set; }
		public int[] AvailableContentTypes { get; set; }
		public string TypeIdentifier { get; set; }
	}
}

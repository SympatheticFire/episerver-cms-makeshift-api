﻿namespace RyzStudio.Web.Episerver
{
	public class IDValue
	{
		public string ID { get; set; }
		public object Value { get; set; }
	}
}

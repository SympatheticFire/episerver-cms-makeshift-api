﻿using System.Collections.Generic;

namespace RyzStudio.Web.Episerver
{
	public class SiteStructure
	{
		public string Name { get; set; }
		public object LanguageID { get; set; }
		public bool IsAvailable { get; set; }
		public bool HasEditAccess { get; set; }
		public string EditURL { get; set; }
		public string URL { get; set; }
		public object URLSegment { get; set; }
		public string UIURL { get; set; }
		public bool IsLanguageNode { get; set; }
		public string Host { get; set; }
		public List<string> Hosts { get; set; }
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp2
{
    public class WebCrawler
    {
        public CookieContainer CookieJar { get; set; }

        public WebCrawler()
        {
            this.CookieJar = new CookieContainer();
        }

        public string Get(string url, string referer)
        {
            if (this.CookieJar == null)
            {
                this.CookieJar = new CookieContainer();
            }

            HttpWebRequest webRequest = (HttpWebRequest)HttpWebRequest.Create(url);
            webRequest.AllowAutoRedirect = true;
            webRequest.Referer = (referer == null) ? url : referer;
            webRequest.Timeout = 10000;
            webRequest.ServerCertificateValidationCallback = delegate { return true; };
            webRequest.CookieContainer = CookieJar;
            webRequest.CachePolicy = new System.Net.Cache.RequestCachePolicy(System.Net.Cache.RequestCacheLevel.NoCacheNoStore);

            WebResponse webResponse;
            try
            {
                webResponse = webRequest.GetResponse();
                if (webResponse == null)
                {
                    return null;
                }

                System.IO.StreamReader sr = new System.IO.StreamReader(webResponse.GetResponseStream());
                return sr.ReadToEnd().Trim();
            }
            catch
            {
                return string.Empty;
            }
        }

        public string Post(string url, string referer, string postData)
        {
            if (this.CookieJar == null)
            {
                this.CookieJar = new CookieContainer();
            }

            HttpWebRequest webRequest = (HttpWebRequest)HttpWebRequest.Create(url);
            webRequest.AllowAutoRedirect = true;
            webRequest.Referer = (referer == null) ? url : referer;
            webRequest.Timeout = 10000;
            webRequest.ServerCertificateValidationCallback = delegate { return true; };
            webRequest.CookieContainer = CookieJar;
            webRequest.CachePolicy = new System.Net.Cache.RequestCachePolicy(System.Net.Cache.RequestCacheLevel.NoCacheNoStore);

            webRequest.ContentType = "application/x-www-form-urlencoded";
            webRequest.Method = "POST";

            if (!string.IsNullOrEmpty(postData))
            {
                byte[] bytes = System.Text.Encoding.ASCII.GetBytes(postData);
                webRequest.ContentLength = bytes.Length;

                System.IO.Stream s = webRequest.GetRequestStream();
                s.Write(bytes, 0, bytes.Length);
                s.Close();
            }
        
            WebResponse webResponse;

            try
            {
                webResponse = webRequest.GetResponse();
                if (webResponse == null)
                {
                    return null;
                }

                System.IO.StreamReader sr = new System.IO.StreamReader(webResponse.GetResponseStream());
                return sr.ReadToEnd().Trim();
            }
            catch
            {
                return string.Empty;
            }
        }

        public void Clear()
        {
            this.CookieJar = new CookieContainer();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RyzStudio.Web.Episerver;

namespace WindowsFormsApp2
{
	public partial class Form1 : Form
	{
		public struct ContentTreeItem
		{
			public int Level { get; set; }
			public string Icon { get; set; }
			public ContentStructure Item { get; set; }			
		}

        protected BackgroundWorker mainWorker = null;
		protected EpiserverCMS episerverCMS = null;

		public Form1()
		{
			InitializeComponent();
		}

		private void button1_Click(object sender, EventArgs e)
		{
			if (episerverCMS == null)
			{
                episerverCMS = new EpiserverCMS
                {
                    BaseURL = textBox1.Text,
                    BaseURLCMS = (string.IsNullOrWhiteSpace(textBox2.Text) ? textBox1.Text : textBox2.Text)
                };
			}

            if (mainWorker == null)
            {
                mainWorker = new BackgroundWorker();
                mainWorker.WorkerReportsProgress = mainWorker.WorkerSupportsCancellation = true;
                mainWorker.DoWork += mainWorker_DoWork;
                mainWorker.RunWorkerCompleted += mainWorker_RunWorkerCompleted;
            }

            if (mainWorker.IsBusy)
            {
                return;
            }

            // clear
            richTextBox1.Text = string.Empty;
            richTextBox1.BackColor = Color.Gainsboro;
			dataGridView1.Rows.Clear();
			dataGridView1.BackgroundColor = Color.Gainsboro;

            bool rv = episerverCMS.Login(textBox4.Text, textBox3.Text);	
			if (rv)
			{
                addLine("Logged In (" + textBox4.Text + ")");
			} else {
                addLine("Not Logged In");
				return;
			}

			button1.Enabled = false;

            mainWorker.RunWorkerAsync();
		}

        private void mainWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            List<ContentTreeItem> contentTreeCollection = new List<ContentTreeItem>();

            addLine("Version = " + episerverCMS.GetVersion());
            addLine("Username = " + episerverCMS.GetProfile_Username());
            addLine(string.Empty);

            addLine("Site Name");
            List<SiteStructure> siteStructureList = episerverCMS.GetSiteStructure();
            if (siteStructureList != null)
            {
                foreach (SiteStructure row in siteStructureList)
                {
                    addLine("  - " + row.Name + " (" + row.URL + ")");
                }
            }

            addLine(string.Empty);

            addLine("Visitor Groups");
            List<IDName> visitorGroupList = episerverCMS.GetVisitorGroup();
            if (siteStructureList != null)
            {
                foreach (IDName row in visitorGroupList)
                {
                    addLine("  - " + row.Name + " (" + row.ID + ")");
                }
            }

            addLine(string.Empty);

            addLine("Channels");
            List<Channel> channelList = episerverCMS.GetChannel_GetAllRegistered();
            if (siteStructureList != null)
            {
                foreach (Channel row in channelList)
                {
                    addLine("  - " + row.Name + " (" + (row.IsActive ? "Active" : "Inactive") + ")");
                }
            }

            addLine(string.Empty);

            addLine("License");
            LicenseInformation licenseInformation = episerverCMS.GetLicenseInformation();
            if (licenseInformation != null)
            {
                addLine("  - " + licenseInformation.IsValid.ToString());
            }

            addLine(string.Empty);

            addLine("Languages");
            List<Language> languagesList = episerverCMS.GetLanguage();
            if (siteStructureList != null)
            {
                foreach (Language row in languagesList)
                {
                    addLine("  - " + row.CultureName + " (" + row.LanguageID + ")");
                }
            }

            addLine(string.Empty);

            addLine("Site Structure");
            if (siteStructureList != null)
            {
                foreach (SiteStructure row in siteStructureList)
                {
                    List<SiteStructure> siteStructureList2 = episerverCMS.GetSiteStructure_SiteURL(row.URL);
                    if (siteStructureList2 != null)
                    {
                        addLine("  - " + row.Name + " (" + row.URL + ")");

                        foreach (SiteStructure row2 in siteStructureList2)
                        {
                            addLine("    - " + row2.Name + " (" + row2.URL + ")");
                        }
                    }
                }
            }

            addLine(string.Empty);

            addLine("Site Structure");

            traversePageTree(ref contentTreeCollection, 0, "1");

            addLine(" = " + contentTreeCollection.Count.ToString());
            addLine("Level\tIcon\tContent ID\tContent GUID\tName\tIs Active\tStatus\tStatus Label\tContentTypeName\tPublic URL\t");

            foreach (ContentTreeItem item in contentTreeCollection)
            {
				addLine(item.Level.ToString() + "\t" + item.Icon + "\t" + item.Item.ContentLink + "\t" + item.Item.ContentGUID + "\t" + item.Item.Name + "\t" + (item.Item.IsDeleted ? "0" : "1") + "\t" + ((int)item.Item.Status).ToString() + "\t" + item.Item.Status.ToString() + "\t" + item.Item.ContentTypeName + "\t" + item.Item.PublicURL + "\t" + item.Item.PreviewURL);

				addRow(new string[] {
					item.Level.ToString(),
					item.Item.ContentLink,
					item.Item.Name,
					(item.Item.IsDeleted ? "0" : "1"),
					((int)item.Item.Status).ToString(),
					item.Item.Status.ToString(),
					item.Item.ContentTypeName,
					item.Item.PublicURL,
					item.Item.PreviewURL
				});
			}
        }

        private void mainWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
			richTextBox1.BackColor = Color.White;
			dataGridView1.BackgroundColor = Color.White;
			button1.Enabled = true;
		}

		protected void addLine(string line)
        {
            if (richTextBox1.InvokeRequired)
            {
                richTextBox1.Invoke(new Action(() => richTextBox1.Text += line + Environment.NewLine));
            }
            else
            {
                richTextBox1.Text += line + Environment.NewLine;
            }
        }

		protected void addRow(string[] cells)
		{
			if (dataGridView1.InvokeRequired)
			{
				dataGridView1.Invoke(new Action(() => dataGridView1.Rows.Add(cells)));
			}
			else
			{
				dataGridView1.Rows.Add(cells);
			}
		}

		protected void traversePageTree(ref List<ContentTreeItem> contentTreeCollection, int level, string pageID)
		{
			List<ContentStructure> contentStructure = episerverCMS.GetContentStructure_GetPageChildren(pageID);
			foreach (ContentStructure item in contentStructure)
			{
				string icon = string.Empty;
				if (item.IsStartPage)
				{
					icon += "H";
				}

				icon += item.Properties.PageShortcutType.ToString();

				contentTreeCollection.Add(new ContentTreeItem
				{
					Level = level,
					Icon = icon,
					Item = item
				});				

				if (item.HasChildren)
				{
					traversePageTree(ref contentTreeCollection, (level + 1), item.ContentLink);
				}
			}

		}
	}
}
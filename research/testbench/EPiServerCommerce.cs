﻿using HtmlAgilityPack;
using System;
using System.Net;
using System.Text;
using System.Web;

namespace WindowsFormsApp2
{
    public class EPiServerCommerce
    {
        public struct ProductCommerceDetails
        {
            public bool ValidInCurrentCulture;
        }

        protected const string commerceURL = @"https://commerce.forevermark.com";       

        public WebCrawler WebCrawler { get; set; }
        public MainForm.OnAddItemDelegate OnAddItem { get; set; }

        public EPiServerCommerce() 
        {
        }

        public EPiServerCommerce(WebCrawler webCrawler)
        {
            this.WebCrawler = webCrawler;
        }

        #region public properties

        public int StoreCount { get; set; }
        public int InactiveStoreCount { get; set; }
        public int ProductCount { get; set; }
        public int InactiveProductCount { get; set; }

        #endregion

        public bool Login(string username, string password)
        {
            string postData = this.buildLoginPostData(username, password);

            this.WebCrawler.CookieJar.Add(new Cookie("GridPageSize_Node-List", "9000", "/", "commerce.forevermark.com"));

            string sourceCode = this.WebCrawler.Post(commerceURL + @"/Apps/Shell/Pages/Login.aspx?ReturnUrl=%2f", null, postData);
            if (string.IsNullOrEmpty(sourceCode))
            {
                return false;
            }

            HtmlAgilityPack.HtmlDocument document = new HtmlAgilityPack.HtmlDocument();
            document.LoadHtml(sourceCode);

            HtmlNode node = null;
            node = document.DocumentNode.SelectSingleNode("//title");
            if (node != null)
            {
                return node.InnerText.Trim().Equals("ForevermarkCommerceManager - EPiServer Commerce Manager", StringComparison.CurrentCultureIgnoreCase);
            }            

            return false;
        }

        public void LoadCatalogProducts(SiteCulture siteCulture)
        {
            this.ProductCount = this.InactiveProductCount = 0;

            string sourceCode = this.WebCrawler.Get(commerceURL + @"/Apps/Shell/Pages/ContentFrame.aspx?_a=Catalog&_v=Node-List&catalognodeid=" + siteCulture.CatalogNodeID.ToString(), null);
            if (string.IsNullOrEmpty(sourceCode))
            {
                return;
            }

            HtmlAgilityPack.HtmlDocument document = new HtmlAgilityPack.HtmlDocument();
            document.LoadHtml(sourceCode);

            HtmlNodeCollection nodeList = document.DocumentNode.SelectNodes("//tr");
            if (nodeList != null)
            {
                foreach (HtmlNode row in nodeList)
                {
                    if (row.Attributes["ibn_primaryKeyId"] == null)
                    {
                        continue;
                    }

                    string productID = row.Attributes["ibn_primaryKeyId"].Value;
                    string productTitle = string.Empty;
                    string productCode = string.Empty;
                    bool isAvailable = false;
                    if (!productID.EndsWith("::Product"))
                    {
                        continue;
                    }

                    productID = productID.Substring(0, productID.IndexOf("::Product"));

                    HtmlNode hnc = row.SelectSingleNode("./td[3]//a");
                    if (hnc != null)
                    {
                        productTitle = decodeUTF8(hnc.InnerText.Trim());
                    }

                    hnc = row.SelectSingleNode("./td[4]//span");
                    if (hnc != null)
                    {
                        productCode = decodeUTF8(hnc.InnerText.Trim());
                    }

                    hnc = row.SelectSingleNode("./td[7]//span");
                    if (hnc != null)
                    {
                        isAvailable = decodeUTF8(hnc.InnerText.Trim()).Trim().Equals("true", StringComparison.CurrentCultureIgnoreCase);
                    }

                    if (isAvailable)
                    {
                        int pid;
                        if (int.TryParse(productID, out pid))
                        {
                            ProductCommerceDetails details = GetProduct(pid, siteCulture.CultureCode);
                            isAvailable = details.ValidInCurrentCulture;
                        }
                    }

                    if (isAvailable)
                    {
                        this.ProductCount++;
                    }
                    else
                    {
                        this.InactiveProductCount++;
                    }

                    if (this.OnAddItem != null)
                    {
                        this.OnAddItem(productID, isAvailable, new string[] { productTitle, productCode, "Yes", string.Empty, string.Empty, string.Empty });
                    }
                }
            }
        }

        public void LoadCatalogStores(int catalogID)
        {
            this.StoreCount = this.InactiveStoreCount = 0;

            string sourceCode = this.WebCrawler.Get(commerceURL + @"/Apps/Shell/Pages/ContentFrame.aspx?_a=Catalog&_v=Node-List&catalognodeid=" + catalogID.ToString(), null);
            if (string.IsNullOrEmpty(sourceCode))
            {
                return;
            }

            HtmlAgilityPack.HtmlDocument document = new HtmlAgilityPack.HtmlDocument();
            document.LoadHtml(sourceCode);

            HtmlNodeCollection nodeList = document.DocumentNode.SelectNodes("//tr");
            if (nodeList != null)
            {
                foreach (HtmlNode row in nodeList)
                {
                    if (row.Attributes["ibn_primaryKeyId"] == null)
                    {
                        continue;
                    }

                    string pid = row.Attributes["ibn_primaryKeyId"].Value;
                    string title = string.Empty;
                    string code = string.Empty;
                    bool isAvailable = false;
                    if (!pid.EndsWith("::Node"))
                    {
                        continue;
                    }

                    pid = pid.Substring(0, pid.IndexOf("::Node"));

                    HtmlNode hnc = row.SelectSingleNode("./td[3]//a");
                    if (hnc != null)
                    {
                        title = decodeUTF8(hnc.InnerText.Trim());
                    }

                    hnc = row.SelectSingleNode("./td[4]//span");
                    if (hnc != null)
                    {
                        code = decodeUTF8(hnc.InnerText.Trim());
                    }

                    hnc = row.SelectSingleNode("./td[7]//span");
                    if (hnc != null)
                    {
                        isAvailable = decodeUTF8(hnc.InnerText.Trim()).Trim().Equals("true", StringComparison.CurrentCultureIgnoreCase);
                    }

                    if (isAvailable)
                    {
                        this.StoreCount++;
                    }
                    else
                    {
                        this.InactiveStoreCount++;
                    }

                    if (this.OnAddItem != null)
                    {
                        this.OnAddItem(pid, isAvailable, new string[] { title, code, "Yes", string.Empty, string.Empty, string.Empty });
                    }
                }
            }
        }

        public ProductCommerceDetails GetProduct(int id, string culture)
        {
            ProductCommerceDetails rs = new ProductCommerceDetails();

            string sourceCode = this.WebCrawler.Get(commerceURL + @"/Apps/Shell/Pages/ContentFrame.aspx?_a=Catalog&_v=Product-Edit&catalogid=0&catalognodeid=0&catalogentryid=" + id.ToString(), null);
            if (string.IsNullOrEmpty(sourceCode))
            {
                return rs;
            }

            HtmlAgilityPack.HtmlDocument document = new HtmlAgilityPack.HtmlDocument();
            document.LoadHtml(sourceCode);

            HtmlNode node = document.DocumentNode.SelectSingleNode("//span[text()='ValidInCurrentCulture (" + culture.Trim().ToLower() + ")']");
            if (node != null)
            {
                node = node.ParentNode.ParentNode;
                if (node != null)
                {
                    HtmlNode node2 = node.SelectSingleNode(".//input[@checked='checked']");
                    rs.ValidInCurrentCulture = ((node2.Attributes["value"] == null) ? string.Empty : node2.Attributes["value"].Value).Equals("true", StringComparison.CurrentCultureIgnoreCase);
                }
            }

            return rs;
        }

        protected string buildLoginPostData(string username, string password)
        {
            string postData = "__LASTFOCUS=&__EVENTTARGET=&__EVENTARGUMENT=";
            string sourceCode = this.WebCrawler.Get(commerceURL, null);
            if (string.IsNullOrEmpty(sourceCode))
            {
                return null;
            }

            HtmlAgilityPack.HtmlDocument document = new HtmlAgilityPack.HtmlDocument();
            document.LoadHtml(sourceCode);

            HtmlNode node = null;
            node = document.DocumentNode.SelectSingleNode("//input[@name='__VIEWSTATE']");
            if (node != null)
            {
                postData += "&__VIEWSTATE=" + HttpUtility.UrlEncode((node.Attributes["value"] == null) ? string.Empty : node.Attributes["value"].Value);
            }

            node = document.DocumentNode.SelectSingleNode("//input[@name='__VIEWSTATEGENERATOR']");
            if (node != null)
            {
                postData += "&__VIEWSTATEGENERATOR=" + HttpUtility.UrlEncode((node.Attributes["value"] == null) ? string.Empty : node.Attributes["value"].Value);
            }

            postData += "&LoginCtrl%24UserName=" + HttpUtility.UrlEncode(username);
            postData += "&LoginCtrl%24Password=" + HttpUtility.UrlEncode(password);
            postData += "&LoginCtrl%24LoginButton=Log+In";

            return postData;
        }

        protected string decodeUTF8(string rawText)
        {
            Encoding utf8 = new UTF8Encoding();
            Encoding window1252 = Encoding.GetEncoding("Windows-1252");

            byte[] postBytes = window1252.GetBytes(rawText.Trim());

            return HttpUtility.HtmlDecode(utf8.GetString(postBytes)).Replace("\n", "").Replace("\r", "").Replace("\t", "").Trim();
        }
    }
}